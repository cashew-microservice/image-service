package com.cashew.image.service.imp;

import com.cashew.image.entities.Image;
import com.cashew.image.repository.ImageRepository;
import com.cashew.image.service.ImageService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class ImageServiceImp implements ImageService {
    private ImageRepository imageRepository;
    private ReactiveMongoTemplate reactiveMongoTemplate;

    public ImageServiceImp( ReactiveMongoTemplate reactiveMongoTemplate, ImageRepository imageRepository){
        this.imageRepository = imageRepository;
        this.reactiveMongoTemplate = reactiveMongoTemplate;
    }
    @Override
    public Flux<Image> findAll() {
        return this.imageRepository.findAll();
    }

    @Override
    public Mono<Image> save(Image image) {
        return imageRepository.save(image);
    }

    @Override
    public Mono<Image>  update(String imageId, Image image) {
        System.out.println("imageId: imageName" + image.getName());
        Query query = new Query(Criteria.where("_id").is(imageId));
        Update update = new Update().set("name", image.getName())
                .set("url", image.getUrl());
        return reactiveMongoTemplate
                .findAndModify(query, update,new FindAndModifyOptions().returnNew(true), Image.class);
    }

    @Override
    public Mono<Image>  findById(String imageId) {
        return imageRepository.findById(imageId);
    }

    @Override
    public Mono<Void> delete(Image image) {
       return imageRepository.delete(image);
    }
}
