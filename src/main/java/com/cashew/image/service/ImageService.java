package com.cashew.image.service;

import com.cashew.image.entities.Image;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ImageService {

    public Flux<Image> findAll();
    public Mono<Image> save(Image image);
    public Mono<Image>  update(String imageId, Image image);
    public Mono<Image>  findById(String imageId);
    public Mono<Void> delete(Image image);

}
