package com.cashew.image.controllers;

import com.cashew.image.entities.Image;
import com.cashew.image.mapper.ImageMapper;
import com.cashew.image.payload.request.ImageCreateRequest;
import com.cashew.image.payload.request.ImageUpdateRequest;
import com.cashew.image.payload.response.ImageResponse;
import com.cashew.image.service.ImageService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.cashew.image.exception.ResourceNotFoundException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/images/v1")
@OpenAPIDefinition(info =
@Info(title = "Images API", version = "1.0", description = "Documentation Images API v1.0")
)
public class ImageController {

    private ImageService imageService;
    private ImageMapper imageMapper;

    public ImageController(ImageService imageService, ImageMapper imageMapper) {
        this.imageService = imageService;
        this.imageMapper = imageMapper;
    }

    @GetMapping
    public Mono<ResponseEntity<List<ImageResponse>>> getImagesV1() {
        return imageService.findAll()
                .map(images -> this.imageMapper.mapToDto(images))
                .collectList()
                .map(body -> ResponseEntity.ok().body(body))
                .switchIfEmpty(Mono.error(new ResourceNotFoundException("Image", "ImageId", 12)));
    }

    @RequestMapping(value = "/{imageId}")
    public Mono<ResponseEntity<ImageResponse>> getImageById(@PathVariable final String imageId) {
        return imageService.findById(imageId)
                .switchIfEmpty(Mono.error(new ResourceNotFoundException("Image", "ImageId", imageId)))
                .map(image -> this.imageMapper.mapToDto(image))
                .map(body -> ResponseEntity.ok().body(body));
    }

    @PostMapping
    public Mono<ResponseEntity> saveImage(@Valid @RequestBody ImageCreateRequest imageCreateRequest, UriComponentsBuilder uriBuilder) {
        return imageService.save(imageMapper.mapToEntity(imageCreateRequest))
                .map(body -> ResponseEntity
                        .created(uriBuilder.path( "/api/images/v1/" + body.getImageId()).buildAndExpand().toUri())
                        .build());
    }

    @PutMapping("/{imageId}")
    public Mono<ResponseEntity> updateImage(@Valid @RequestBody ImageUpdateRequest imageUpdateRequest, @PathVariable String imageId) {

        return imageService.findById(imageId)
                .flatMap( image -> imageService.update(imageId, imageMapper.mapToEntity(imageUpdateRequest)))
                .switchIfEmpty(Mono.error(new ResourceNotFoundException("Image", "ImageId", imageId)))
                .map(imageDto -> this.imageMapper.mapToDto(imageDto))
                .map(body -> ResponseEntity.ok().build());
    }

    @DeleteMapping("/{imageId}")
    public Mono<ResponseEntity<Void>> deleteImage(@PathVariable String imageId) {
        return imageService.findById(imageId)
                .switchIfEmpty(Mono.error(new ResourceNotFoundException("Image", "ImageId", imageId)))
                .map(image -> imageService.delete(image).subscribe())
                .map(body -> ResponseEntity.noContent().build());
    }

}
