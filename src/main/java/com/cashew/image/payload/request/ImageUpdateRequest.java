package com.cashew.image.payload.request;

import lombok.Data;
import lombok.NonNull;

@Data
public class ImageUpdateRequest {
    @NonNull
    private String name;
    @NonNull
    private String url;
}
