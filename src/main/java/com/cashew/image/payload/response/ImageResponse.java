package com.cashew.image.payload.response;

import lombok.Data;

@Data
public class ImageResponse {
    private String imageId;
    private String name;
    private String url;
}
