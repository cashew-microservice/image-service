package com.cashew.image.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Image {
    @Id
    private String imageId;
    private String name;
    private String url;

    public Image(String name, String url) {
        this.name= name;
        this.url= url;
    }
}
