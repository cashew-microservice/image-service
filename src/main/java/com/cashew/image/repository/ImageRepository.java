package com.cashew.image.repository;

import com.cashew.image.entities.Image;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ImageRepository  extends ReactiveMongoRepository<Image, String> {
}
