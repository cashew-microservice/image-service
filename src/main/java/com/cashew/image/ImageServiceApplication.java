package com.cashew.image;

import com.cashew.image.entities.Image;
import com.cashew.image.repository.ImageRepository;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableEurekaClient
@EnableReactiveMongoRepositories
public class ImageServiceApplication implements CommandLineRunner {

	@Autowired
	private ImageRepository imageRepository;

    public static void main(String[] args) {
    	SpringApplication.run(ImageServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    	imageRepository.findAll().switchIfEmpty( images -> {
			System.out.println("Initializing DB ");
			List<Image> newImages = Arrays.asList(
					new Image( "Treehouse of Horror V", "https://www.imdb.com/title/tt0096697/mediaviewer/rm3842005760"),
					new Image( "The Town", "https://www.imdb.com/title/tt0096697/mediaviewer/rm3698134272"),
					new Image( "The Last Traction Hero", "https://www.imdb.com/title/tt0096697/mediaviewer/rm1445594112"));
			imageRepository.saveAll(newImages).subscribe();
		}).subscribe();

    }
}
