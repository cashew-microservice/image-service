package com.cashew.image.mapper;

import com.cashew.image.entities.Image;
import com.cashew.image.payload.request.ImageCreateRequest;
import com.cashew.image.payload.request.ImageUpdateRequest;
import com.cashew.image.payload.response.ImageResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class ImageMapper {
    private ModelMapper modelMapper;

    public ImageMapper(ModelMapper modelMapper){
        this.modelMapper = modelMapper;
    }
    //can be static method
    public List<ImageResponse> mapToDto(List<Image> images) {
        return mapList(images, ImageResponse.class);
    }

    public ImageResponse mapToDto(Image image) {
        ImageResponse imageResponse = modelMapper.map(image, ImageResponse.class);
        return imageResponse;
    }

    public Image mapToEntity(ImageCreateRequest imageCreateRequest) {
        Image image = modelMapper.map(imageCreateRequest, Image.class);
        return image;
    }

    public Image mapToEntity(ImageUpdateRequest imageUpdateRequest) {
        Image image = modelMapper.map(imageUpdateRequest, Image.class);
        return image;
    }

    <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }

}
